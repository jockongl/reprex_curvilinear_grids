# reprEx_curvilinear_grids

Code and data needed for a reproducible example as part of a [question posted on stack overflow](https://stackoverflow.com/questions/78543998/regridding-two-different-curvilinear-grids-onto-a-common-rectilinear-grid-with-r) to regrid two curvilinear grids onto a single rectilinear grid (that is higher resolution).

